# OkayWL
A moduler Wayland compositor written in FORTH.
## Features
External program manages tiling. <br>
Four types of window managers will be created: <br>
* Hyprland (Dwindle)-like <br>
* DWM-like <br>
* StumpWM-like <br>
* Openbox-like <br>

Titlebars, and window decorations can be displayed on windows. These titlebars will be created with an external program like eww.

Keybindings are managed with an external program.

The Wayland compositor will have plugins/patches for animations, blur, shadows, rounded corners, and other such effects.

The Wayland compositor will be configured with a script similar to River.

The Wayland compositor will have an API that supports advanced control commands and scripts for moving windows.

Workspaces are created with a command. That way, they can be created staticly or dynamicly.
